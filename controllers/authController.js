const jwt = require('jsonwebtoken');
const User = require('../models/User');
const config = require('../config/default');
const {SUCCESS_CODE, BAD_REQUEST_CODE, INTERNAL_SERVER_ERROR_CODE} = require('../config/default');


module.exports.register = async (req, res) => {
    try {
        const {username, password} = req.body;
        if (!username || !password) {
            return res.status(BAD_REQUEST_CODE).json({message: 'Invalid credentials'});
        }
        const user = new User({username, password});
        await user.save()
        return res.json({message: 'Success'});
    } catch (e) {
        return res.status(500).json({message: e.message});
    }

};

module.exports.login = async (req, res) => {
    try {
        const {username, password} = req.body;
        if (!username || !password) {
            return res.status(BAD_REQUEST_CODE).json({message: 'Invalid credentials'});
        }
        const user = await User.findOne({username, password}).exec()
        if (!user) {
            return res.status(BAD_REQUEST_CODE).json({status: 'No user with such username and password found'});
        }
        return res.json({
            status: 'success',
            jwt_token: jwt.sign(JSON.stringify({
                _id: user.id,
                username: user.username,
                createdDate: user.createdDate
            }), config.secret)
        });

    } catch (e) {
        return res.status(500).json({message: e.message});
    }
};


