const Note = require('../models/note');
const {BAD_REQUEST_CODE, INTERNAL_SERVER_ERROR_CODE} = require('../config/default');


module.exports.getNote = async (req, res) => {
    try {
        const note = await Note.findOne({_id: req.params.id}).exec();
        if (!note) {
            return res.status(BAD_REQUEST_CODE).json({message: 'No note found by this id'});
        }
        if (note.userId != req.user._id) {
            return res.status(BAD_REQUEST_CODE).json({message: 'You have no access to this note'});
        }
        return res.json({note: note});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not get note details'});
    }
};

module.exports.getNotes = async (req, res) => {
    try {
        let notes = await Note.find({userId: req.user._id});
        return res.json({notes: notes});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not get notes'});
    }
};

module.exports.createNote = async (req, res) => {
    try {
        if (!req.body.text) {
            return res.status(BAD_REQUEST_CODE).json({message: 'No text in body'});
        }
        const note = new Note({
            userId: req.user._id,
            text: req.body.text
        });
        await note.save();
        return res.json({message: 'Success'});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not add note'});
    }
};



module.exports.editNote = async (req, res) => {
    try {
        let note = await Note.findOne({_id: req.params.id}).exec();
        if (!note) {
            return res.status(BAD_REQUEST_CODE).json({message: 'No note found'});
        }
        if (note.userId != req.user._id) {
            return res.status(BAD_REQUEST_CODE).json({message: 'You have no access to this note'});
        }
        await Note.findByIdAndUpdate({_id: req.params.id}, {text: req.body.text});
        return res.json({message: "Success"});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not edit note'});
    }
};

module.exports.deleteNote = async (req, res) => {
    try {
        let note = await Note.findOne({_id: req.params.id}).exec();
        if (!note) {
            return res.status(BAD_REQUEST_CODE).json({message: 'No note found'});
        }
        if (note.userId != req.user._id) {
            return res.status(BAD_REQUEST_CODE).json({message: 'You have no access to this note'});
        }
        await Note.findByIdAndDelete({_id: req.params.id});
        return res.json({message: "Success"});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not delete note'});
    }
};

module.exports.checkNote = async (req, res) => {
    try {
        let note = await Note.findOne({_id: req.params.id}).exec();
        if (!note) {
            return res.status(BAD_REQUEST_CODE).json({message: 'No note found'});
        }
        await Note.findByIdAndUpdate({_id: req.params.id}, {completed: !note.completed});
        return res.json({message: "Success"});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not check note'});
    }
};



