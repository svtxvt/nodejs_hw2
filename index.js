const express = require('express');
const mongoose = require('mongoose');
const app = express();
const config = require('./config/default');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');


const {BAD_REQUEST_CODE, INTERNAL_SERVER_ERROR_CODE} = require('./config/default');

mongoose.connect(`mongodb+srv://test:test@cluster0.by3rw.mongodb.net/lab2?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);
app.use('*', (req, res) =>
    res.status(BAD_REQUEST_CODE).json({message: 'Router not found'})
)

app.listen(config.port, () => {
    console.log(`Server listens on ${config.port} port`);
});
