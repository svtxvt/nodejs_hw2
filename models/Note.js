const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Types;

module.exports = mongoose.model('Note', new Schema(
    {
        userId: {type: Types.ObjectId, ref: 'User'},
        completed: {type: Boolean, required: true, default: false},
        text: {type: String, required: true},
        createdDate: {type: Date, required: true, default: Date.now}
    },
    {versionKey: false}
));
