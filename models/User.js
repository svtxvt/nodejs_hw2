const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('user', new Schema({
        username: {
            required: true,
            type: String,
            unique: true
        },
        password: {
            required: true,
            type: String
        },
        createdDate: {
            type: Date,
            required: true,
            default: Date.now
        }
    },
    {versionKey: false}
));