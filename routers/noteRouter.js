const express = require('express');
const router = express.Router();

const {getNotes, createNote, getNote, editNote, checkNote, deleteNote} = require('../controllers/noteController');
const authMiddleware = require('../middlewares/authMiddleware');

router.get('/notes', authMiddleware, getNotes);
router.post('/notes', authMiddleware, createNote);
router.get('/notes/:id', authMiddleware, getNote);
router.put('/notes/:id', authMiddleware, editNote);
router.patch('/notes/:id', authMiddleware, checkNote);
router.delete('/notes/:id', authMiddleware, deleteNote);


module.exports = router;